<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta content="width=device-width, initial-scale=1.0" name="viewport">

	<title>Sistem Klasemen Sepak Bola - Landing Page</title>
	<meta content="" name="description">
	<meta content="" name="keywords">

	<!-- Favicons -->
	<link href="#" rel="icon">
	<link href="<?= base_url('assets/assets') ?>/img/apple-touch-icon.png" rel="apple-touch-icon">

	<!-- Google Fonts -->
	<link
		href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Dosis:300,400,500,,600,700,700i|Lato:300,300i,400,400i,700,700i"
		rel="stylesheet">

	<!-- Vendor CSS Files -->
	<link href="<?= base_url('assets/assets') ?>/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?= base_url('assets/assets') ?>/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
	<link href="<?= base_url('assets/assets') ?>/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
	<link href="<?= base_url('assets/assets') ?>/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
	<link href="<?= base_url('assets/assets') ?>/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

	<!-- Template Main CSS File -->
	<link href="<?= base_url('assets/assets') ?>/css/style.css" rel="stylesheet">

	<!-- =======================================================
  * Template Name: Butterfly - v4.7.0
  * Template URL: https://bootstrapmade.com/butterfly-free-bootstrap-theme/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

	<!-- ======= Header ======= -->
	<header id="header" class="fixed-top">
		<div class="container d-flex align-items-center justify-content-between">

			<a href="<?= base_url(); ?>" class="logo">SIKAS</a>
			<!-- Uncomment below if you prefer to use text as a logo -->
			<!-- <h1 class="logo"><a href="index.html">Butterfly</a></h1> -->

			<nav id="navbar" class="navbar">
				<ul>
					<li><a class="nav-link scrollto" href="#hero">Home</a></li>
					<li><a class="nav-link scrollto" href="<?= base_url('auth/index'); ?>">Login</a></li>
				</ul>
				<i class="bi bi-list mobile-nav-toggle"></i>
			</nav><!-- .navbar -->

		</div>
	</header><!-- End Header -->

	<!-- ======= Hero Section ======= -->
	<section id="hero" class="d-flex align-items-center">

		<div class="container" style="margin-top: 80px;">
			<div class="row">
				<div class="col-lg-6 pt-4 pt-lg-0 order-2 order-lg-1 d-flex flex-column justify-content-center">
					<h1>Aplikasi Sistem Informasi Bimbingan Proyek</h1>
					<h2>Sistem informasi bimbingan proyek ini merupakan aplikasi yang digunakan mahasiswa Politeknik Pos Indonesia
						khususnya pada program studi D3 Teknik Informatika untuk mempermudah proses bimbingan proyek Tahun Ajaran 2021/2022.</h2>
					<div class="row">
						<div><a href="<?= base_url('auth/registration'); ?>" class="btn-get-started scrollto">Register</a></div>
					</div>
				</div>
				<div class="col-lg-6 order-1 order-lg-2 hero-img">
					<img src="<?= base_url('assets/assets') ?>/img/hero-img.png" class="img-fluid" alt="">
				</div>
			</div>
		</div>

	</section><!-- End Hero -->

	<main id="main">


		<!-- ======= Footer ======= -->
		<footer id="footer">

			<div class="footer-top">
			</div>

			<div class="container py-4">
				<div class="copyright">
					&copy; Tomi Cahya Anugrah,  <?= date('Y'); ?>
				</div>
				<div class="credits">
					<!-- All the links in the footer should remain intact. -->
					<!-- You can delete the links only if you purchased the pro version. -->
					<!-- Licensing information: https://bootstrapmade.com/license/ -->
					<!-- Purchase the pro version with working =/AJAX contact form: https://bootstrapmade.com/butterfly-free-bootstrap-theme/ -->
					Designed by <a href="https://github.com/raihanmalikul/Sistem_Informasi_Bimbingan">SIKAS</a>
				</div>
			</div>
		</footer><!-- End Footer -->

		<a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i
				class="bi bi-arrow-up-short"></i></a>

		<!-- Vendor JS Files -->
		<script src="<?= base_url('assets/assets') ?>/vendor/purecounter/purecounter.js"></script>
		<script src="<?= base_url('assets/assets') ?>/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
		<script src="<?= base_url('assets/assets') ?>/vendor/glightbox/js/glightbox.min.js"></script>
		<script src="<?= base_url('assets/assets') ?>/vendor/isotope-layout/isotope.pkgd.min.js"></script>
		<script src="<?= base_url('assets/assets') ?>/vendor/swiper/swiper-bundle.min.js"></script>
		<script src="<?= base_url('assets/assets') ?>/vendor/=-email-form/validate.js"></script>

		<!-- Template Main JS File -->
		<script src="<?= base_url('assets/assets') ?>/js/main.js"></script>

</body>

</html>
