<!-- Kuisioner Jawaban View -->
<div class="dashboard-wrapper">
    <div class="container-fluid dashboard-content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Judul : <?= $kuisioner->judul; ?></h4>
                        <p class="card-text">Deskripsi : <?= $kuisioner->deskripsi; ?></p>
                        <p class="card-text">Kategori: <?= $kuisioner->kategori; ?></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h5>Jawaban oleh Pengguna:</h5>
                        <!-- <div class="col-md-12"> -->
                            <a href="<?= base_url('kuisioner/print/' . $this->uri->segment(4) . '/' . $this->uri->segment(5)); ?>" class="btn btn-danger float-right btn-sm mb-2"><i class="glyphicon glyphicon"></i> Print PDF</a>
                            <a href="<?= base_url('kuisioner/export/' . $this->uri->segment(4) . '/' . $this->uri->segment(5)); ?>" class="btn btn-success float-right btn-sm mb-2 mr-3"><i class="glyphicon glyphicon"></i> Print Excel</a>
                        <!-- </div> -->
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Pertanyaan</th>
                                        <th>Jawaban</th>
                                        <th>Skor</th>
                                        <th>Keterangan</th> <!-- Add this column for description -->
                                        <th>File Upload</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($jawaban as $item) : ?>
                                        <tr>
                                            <td><?= $item->pertanyaan; ?></td>
                                            <td><?= $item->jawaban; ?></td>
                                            <td><?= $item->skor; ?></td>
                                            <td>
                                                <?php
                                                $keterangan = "";
                                                if ($item->skor < 40) {
                                                    $keterangan = "KURANG";
                                                } elseif ($item->skor >= 40 && $item->skor < 60) {
                                                    $keterangan = "CUKUP";
                                                } elseif ($item->skor >= 60 && $item->skor < 80) {
                                                    $keterangan = "BAIK";
                                                } elseif ($item->skor >= 80) {
                                                    $keterangan = "BAIK SEKALI";
                                                }
                                                echo $keterangan;
                                                ?>
                                            </td>
                                            <td>
                                                <?php if (isset($item->file_upload)) : ?>
                                                    <a href="<?= base_url($item->file_upload); ?>" class="btn btn-primary" download>Download File</a>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <a href="<?= base_url('kuisioner'); ?>" class="btn btn-secondary">Kembali ke Daftar Kuisioner</a>
            </div>
        </div>
    </div>
</div>
<!-- End Kuisioner Jawaban View -->
