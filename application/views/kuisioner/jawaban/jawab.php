<div class="dashboard-wrapper">
    <div class="container-fluid dashboard-content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <?php if (!empty($message)) : ?>
                            <div class="alert alert-success" role="alert">
                                Anda telah mengisi kuisioner ini.
                            </div>
                        <?php endif; ?>

                        <form method="post" enctype="multipart/form-data" action="<?= base_url('kuisioner/jawab_submit'); ?>">
                            <input type="hidden" value="<?= $kuisioner_id; ?>" name="kuisioner_id">
                            <?php foreach ($pertanyaan as $item) : ?>
                                <div class="form-group">
                                    <label for="jawaban_<?= $item->id; ?>"><?= $item->pertanyaan; ?></label>
                                    <textarea name="jawaban[<?= $item->id; ?>]" id="jawaban_<?= $item->id; ?>" class="form-control" rows="4" cols="50"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="skor_<?= $item->id; ?>">Skor</label>
                                    <input type="number" name="skor[<?= $item->id; ?>]" id="skor_<?= $item->id; ?>" class="form-control" min="0" max="100" required>
                                </div>
                                <input type="file" class="form-control" name="file_upload[<?= $item->id ?>]">
                            <?php endforeach; ?>


                            <div class="form-group">
                                <button type="submit" value="Submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <a href="<?= base_url('kuisioner'); ?>" class="btn btn-secondary">Kembali ke Daftar Kuisioner</a>
            </div>
        </div>
    </div>
</div>