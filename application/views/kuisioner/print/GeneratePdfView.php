<!DOCTYPE html>
<html>

<head>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta charset="utf-8">
    <title>Hasil Kuisioner</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" type="text/css" rel="stylesheet" />
</head>

<body>
    <h1 class="text-center bg-info">Hasil Kuisioner</h1>
    <table class="table">
        <tbody>
            <?php foreach ($kuisioner as $item) : ?>
                <tr>
                    <td>Judul</td>
                    <td>:</td>
                    <th><?= $item->judul; ?></th>
                </tr>
                <tr>
                    <td>Deskripsi</td>
                    <td>:</td>
                    <th><?= $item->deskripsi; ?></th>
                </tr>
                <tr>
                    <td>Kategori</td>
                    <td>:</td>
                    <th><?= $item->kategori; ?></th>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <table class="table table-striped table-hover">
        <thead>
            <tr>
                <th>Pertanyaan</th>
                <th>Jawaban</th>
                <th>Skor</th>
                <th>Keterangan</th> <!-- Add this column for description -->
                <!-- <th>File Upload</th> -->
            </tr>
        </thead>
        <tbody>
            <?php foreach ($jawaban as $item) : ?>
                <tr>
                    <td><?= $item->pertanyaan; ?></td>
                    <td><?= $item->jawaban; ?></td>
                    <td><?= $item->skor; ?></td>
                    <td>
                        <?php
                        $keterangan = "";
                        if ($item->skor < 40) {
                            $keterangan = "KURANG";
                        } elseif ($item->skor >= 40 && $item->skor < 60) {
                            $keterangan = "CUKUP";
                        } elseif ($item->skor >= 60 && $item->skor < 80) {
                            $keterangan = "BAIK";
                        } elseif ($item->skor >= 80) {
                            $keterangan = "BAIK SEKALI";
                        }
                        echo $keterangan;
                        ?>
                    </td>
                    <!-- <td>
                                                <?php if (isset($item->file_upload)) : ?>
                                                    <a href="<?= base_url($item->file_upload); ?>" class="btn btn-primary" download>Download File</a>
                                                <?php endif; ?>
                                            </td> -->
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</body>

</html>