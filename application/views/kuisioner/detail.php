<!-- Kuisioner Jawaban View -->
<div class="dashboard-wrapper">
    <div class="container-fluid dashboard-content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                    <h4 class="card-title">Judul : <?= $kuisioner->judul; ?></h4>
                        <p class="card-text">Deskripsi : <?= $kuisioner->deskripsi; ?></p>
                        <p class="card-text">Kategori: <?= $kuisioner->kategori; ?></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h5>Pengguna yang Telah Menjawab:</h5>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>User ID</th>
                                        <th>Total Skor</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($users as $user) : ?>
                                        <tr>
                                            <td><?= $user->userId; ?></td>
                                            <td><?= $user->total_skor ;?>
                                            </td>
                                            <td>
                                                <a href="<?= base_url('kuisioner/jawaban/view/') . $kuisioner->id . '/' . $user->user_id; ?>" class="btn btn-primary">Lihat Jawaban</a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <a href="<?= base_url('kuisioner'); ?>" class="btn btn-secondary">Kembali ke Daftar Kuisioner</a>
            </div>
        </div>
    </div>
</div>
<!-- End Kuisioner Jawaban View -->
