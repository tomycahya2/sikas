<div class="dashboard-wrapper">
    <div class="container-fluid dashboard-content">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title">Edit Kuisioner</h2>
                    <hr>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">

                <div class="card">
                    <div class="card-body">
                        <form method="post" action="<?= base_url('kuisioner/update/' . $kuisioner->id); ?>">
                            <div class="form-group">
                                <label for="judul">Judul:</label>
                                <input type="text" name="judul" id="judul" class="form-control" value="<?= set_value('judul', $kuisioner->judul); ?>">
                                <?= form_error('judul'); ?>
                            </div>

                            <div class="form-group">
                                <label for="deskripsi">Deskripsi:</label>
                                <textarea name="deskripsi" class="form-control" id="deskripsi"><?= set_value('deskripsi', $kuisioner->deskripsi); ?></textarea>
                                <?= form_error('deskripsi'); ?>
                            </div>


                            <div class="form-group">
                                <label for="kategori_id">Kategori:</label>
                                <select class="form-control" name="kategori_id" id="kategori_id">
                                    <option value="">Pilih Kategori</option>
                                    <?php foreach ($kategori as $item) : ?>
                                        <option value="<?= $item->id; ?>" <?= ($item->id == $kuisioner->kategori_id) ? 'selected' : ''; ?>><?= $item->kategori; ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <?= form_error('kategori_id'); ?>
                            </div>

                            <div class="form-group">
                                <button class="btn btn-primary" type="submit" value="Simpan">Submit</button>
                            </div>
                        </form>

                        <a class="btn btn-light" href="<?= base_url('kuisioner'); ?>">Kembali ke Daftar Kuisioner</a>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>