<div class="dashboard-wrapper">
    <div class="container-fluid dashboard-content">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title">Kuisioner</h2>
                    <hr>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="btn-group mb-3">
                            <a class="btn btn-light" href="<?= base_url('kuisioner'); ?>">Kembali</a>
                        </div>
                        <form method="post" action="<?= base_url('kuisioner/store'); ?>">
                            <div class="form-group">
                                <label for="judul">Judul:</label>
                                <input type="text" name="judul" id="judul" class="form-control" value="<?= set_value('judul'); ?>">
                                <?= form_error('judul'); ?>
                            </div>

                            <div class="form-group">
                                <label for="deskripsi">Deskripsi:</label>
                                <textarea name="deskripsi" id="deskripsi" class="form-control"><?= set_value('deskripsi'); ?></textarea>
                                <?= form_error('deskripsi'); ?>
                            </div>

                            <div class="form-group">
                                <label for="kategori_id">Kategori:</label>
                                <select name="kategori_id" id="kategori_id" class="form-control">
                                    <option value="">Pilih Kategori</option>
                                    <?php foreach ($kategori as $item) : ?>
                                        <option value="<?= $item->id; ?>"><?= $item->kategori; ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <?= form_error('kategori_id'); ?>
                            </div>

                            <div class="form-group">
                                <input type="submit" value="Simpan" class="btn btn-primary">
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>