<div class="dashboard-wrapper">
    <div class="container-fluid dashboard-content">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title"><?= $kuisioner->judul; ?></h2>
                    <hr>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">

                        <div class="btn-group mb-3">
                            <a class="btn btn-light" href="<?= base_url('kuisioner'); ?>">Kembali ke Daftar Kuisioner</a>

                            <!-- <a class="btn btn-primary " href="<?= base_url('kuisioner/pertanyaan/create/' . $kuisioner->id); ?>">Buat Pertanyaan Baru</a> -->

                        </div>

                        <form method="post" action="<?= base_url('kuisioner_pertanyaan/store'); ?>">
                            <input type="hidden" name="kuisioner_id" value="<?= $kuisioner->id; ?>">

                            <div class="form-group">
                                <label for="pertanyaan">Pertanyaan:</label>
                                <textarea name="pertanyaan" id="pertanyaan" class="form-control"><?= set_value('pertanyaan'); ?></textarea>
                                <?= form_error('pertanyaan'); ?>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Simpan Pertanyaan</button>
                            </div>
                        </form>


                        <div class="table-responsive">
                            <table class="table">
                                <tr>
                                    <th>Pertanyaan</th>
                                    <th>Aksi</th>
                                </tr>
                                <?php foreach ($pertanyaan as $item) : ?>
                                    <tr>
                                        <td><?= $item->pertanyaan; ?></td>
                                        <td>
                                            <a class="btn btn-sm btn-success" href="<?= base_url('kuisioner/pertanyaan/edit/' . $item->id); ?>">Edit</a>
                                            <a class="btn btn-sm btn-danger" href="<?= base_url('kuisioner/pertanyaan/delete/' . $item->id); ?>">Hapus</a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>