<div class="dashboard-wrapper">
    <div class="container-fluid dashboard-content">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title">Buat Pertanyaan Baru</h2>
                    <hr>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <form method="post" action="<?= base_url('kuisioner_pertanyaan/store'); ?>">
                            <input type="hidden" name="kuisioner_id" value="<?= $kuisioner_id; ?>">

                            <div class="form-group">
                                <label for="pertanyaan">Pertanyaan:</label>
                                <textarea name="pertanyaan" id="pertanyaan" class="form-control"><?= set_value('pertanyaan'); ?></textarea>
                                <?= form_error('pertanyaan'); ?>
                            </div>

                            <div class="form-group">
                                <input type="submit" value="Simpan" class="btn btn-primary">
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>

        <a href="<?= base_url('kuisioner_pertanyaan/index/' . $kuisioner_id); ?>">Kembali ke Daftar Pertanyaan Kuisioner</a>
        </body>

        </html>