<div class="dashboard-wrapper">
    <div class="container-fluid dashboard-content">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title">Edit Pertanyaan</h2>
                    <hr>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="btn-group mb-3">
                            <a class="btn btn-light" href="<?= base_url('kuisioner_pertanyaan/index/' . $pertanyaan->kuisioner_id); ?>">Kembali ke Daftar Pertanyaan Kuisioner</a>
                        </div>

                        <form method="post" action="<?= base_url('kuisioner_pertanyaan/update/' . $pertanyaan->id); ?>">
                            <input type="hidden" name="kuisioner_id" value="<?= $pertanyaan->kuisioner_id; ?>">

                            <div class="form-group">
                                <label for="pertanyaan">Pertanyaan:</label>
                                <textarea name="pertanyaan" id="pertanyaan" class="form-control"><?= set_value('pertanyaan', $pertanyaan->pertanyaan); ?></textarea>
                                <?= form_error('pertanyaan'); ?>
                            </div>

                            <div class="form-group">
                                <button type="submit" value="Simpan" class="btn btn-primary">Submit</button>
                            </div>
                        </form>

                    </div>
                </div>

            </div>
        </div>
    </div>

</div>