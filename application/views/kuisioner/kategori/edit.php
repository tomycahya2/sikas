<div class="dashboard-wrapper">
    <div class="container-fluid dashboard-content">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title">Edit Kategori</h2>
                    <hr>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="btn-group mb-3">

                            <a class="btn btn-light" href="<?= base_url('kuisioner/kategori'); ?>">Kembali </a>
                        </div>
                        <form method="post" action="<?= base_url('kuisioner/kategori/update/' . $kategori->id); ?>">
                            <div class="form-group">
                                <label for="kategori">Kategori:</label>
                                <input type="text" name="kategori" id="kategori" class="form-control" value="<?= set_value('kategori', $kategori->kategori); ?>">
                                <?= form_error('kategori'); ?>
                            </div>

                            <div class="form-group">
                                <button type="submit" value="Simpan" class="btn btn-primary">Submit</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>