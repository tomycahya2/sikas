<div class="dashboard-wrapper">
    <div class="container-fluid dashboard-content">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title">Kuisioner Kategori</h2>
                    <hr>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="btn-group mb-3">
                            <a class="btn btn-light" href="<?= base_url('kuisioner'); ?>">Kembali</a>
                            <a class="btn btn-primary" href="<?= base_url('kuisioner/kategori/create'); ?>">Buat Kategori Baru</a>
                        </div>

                        <div class="table-responsive">
                            <table class="table ">
                                <tr>
                                    <th>ID</th>
                                    <th>Kategori</th>
                                    <th>Aksi</th>
                                </tr>
                                <?php foreach ($kategori as $item) : ?>
                                    <tr>
                                        <td><?= $item->id; ?></td>
                                        <td><?= $item->kategori; ?></td>
                                        <td>
                                            <a class="btn btn-sm btn-success" href="<?= base_url('kuisioner/kategori/edit/' . $item->id); ?>">Edit</a>
                                            <a class="btn btn-sm btn-danger" href="<?= base_url('kuisioner/kategori/delete/' . $item->id); ?>">Hapus</a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>