<?php
$role_id = $this->session->userdata('role_id');
$role = $this->db->get_where('user_role', ['id' => $role_id])->row_array();
// print_r($role);exit;
?>
<div class="dashboard-wrapper">
    <div class="container-fluid dashboard-content">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title">Kuisioner</h2>
                    <hr>
                </div>
            </div>
        </div>
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="btn-group mb-3">
                            <a class="btn btn-primary" href="<?= base_url('kuisioner/create'); ?>">Buat Kuisioner Baru</a>
                            <a class="btn btn-dark" href="<?= base_url('kuisioner/kategori'); ?>">Kelola Kategori</a>
                        </div>
                        <div class="table-responsive">

                            <table class="table">
                                <tr>
                                    <th>No</th>
                                    <th>Judul</th>
                                    <th>Kategori</th>
                                    <th>Jml Pertanyaan</th>
                                    <th>Penjawab</th>
                                    <th>Aksi</th>
                                </tr>
                                <?php
                                $no = 1;
                                foreach ($kuisioner as $item) : ?>
                                    <tr>
                                        <td><?= $no++; ?></td>
                                        <td><?= $item->judul; ?></td>
                                        <td><?= $item->kategori; ?></td>
                                        <td><?= $item->jumlah_pertanyaan; ?></td>
                                        <td><?= $item->jumlah_pengguna; ?></td>
                                        <td>
                                            <?php if($role['id'] == '1' || $role['id'] == '2'){
                                                ?>
                                                    <a class="btn btn-sm btn-dark" href="<?= base_url('kuisioner/pertanyaan/' . $item->id); ?>">Manage</a>
                                                    <a class="btn btn-sm btn-light" href="<?= base_url('kuisioner/detail/' . $item->id); ?>">Lihat Hasil</a>
                                                    <a class="btn btn-sm btn-light" href="<?= base_url('kuisioner/jawaban/' . $item->id); ?>">Jawab</a>
                                                
                                                    <a class="btn btn-sm btn-success" href="<?= base_url('kuisioner/edit/' . $item->id); ?>">Edit</a>
                                                    <a class="btn btn-sm btn-danger" href="<?= base_url('kuisioner/delete/' . $item->id); ?>">Hapus</a>
                                                <?php
                                            }else if($role['id'] == '3'){
                                                ?>
                                                    <a class="btn btn-sm btn-light" href="<?= base_url('kuisioner/detail/' . $item->id); ?>">Lihat Hasil</a>
                                                    <a class="btn btn-sm btn-light" href="<?= base_url('kuisioner/jawaban/' . $item->id); ?>">Jawab</a>
                                                <?php
                                            } ?>
                                            
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>