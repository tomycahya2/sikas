<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kuisioner_jawab extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Kuisioner_jawaban_model');
        $this->load->model('Kuisioner_model');
    }

    public function jawab($kuisioner_id)
    {
        $data['user'] = $this->db->get_where('user', ['user_id' =>
        $this->session->userdata('user_id')])->row_array();
        $data['user_data'] = $this->db->get_where('user_data', ['data_id' =>
        $data['user']['data_id']])->row_array();
        $data['admin'] = $this->db->get_where('admin', ['dos_id' =>
        $data['user']['dos_id']])->row_array();
        // Mendapatkan daftar pertanyaan kuisioner berdasarkan kuisioner_id
        $data['pertanyaan'] = $this->Kuisioner_jawaban_model->get_pertanyaan_by_kuisioner_id($kuisioner_id);

        // Periksa apakah pengguna sudah mengisi kuisioner untuk pertanyaan-pertanyaan ini
        $user_id = $this->session->userdata('get_user_id');
        $answeredPertanyaan = [];

        foreach ($data['pertanyaan'] as $pertanyaan) {
            if ($this->Kuisioner_jawaban_model->hasAnswered($pertanyaan->id, $user_id)) {
                $answeredPertanyaan[] = $pertanyaan->id;
            }
        }

        $data['answered_pertanyaan'] = $answeredPertanyaan;

        $data['title'] = 'Kuisioner';
        $data['kuisioner_id'] = $kuisioner_id;
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar_1', $data);
        $this->load->view('kuisioner/jawaban/jawab', $data);
        $this->load->view('templates/footer');
    }

    public function submit_jawaban()
    {
        try {
            // Validate the form (customize validation rules as needed)
            $this->form_validation->set_rules('jawaban[]', 'Jawaban', 'required');
            $this->form_validation->set_rules('skor[]', 'Skor', 'required'); // Add validation for 'skor'
            
            $user_id = $this->session->userdata('get_user_id');
            $kuisioner_id = $this->input->post('kuisioner_id');
    
            if ($this->form_validation->run() === FALSE) {
                // Handle form validation errors as needed
                $data['pertanyaan'] = $this->Kuisioner_jawaban_model->get_pertanyaan_by_kuisioner_id($kuisioner_id);
    
                // Check if the user has already answered some questions
                $answeredPertanyaan = [];
    
                foreach ($data['pertanyaan'] as $pertanyaan) {
                    if ($this->Kuisioner_jawaban_model->hasAnswered($pertanyaan->id, $user_id)) {
                        $answeredPertanyaan[] = $pertanyaan->id;
                    }
                }
    
                $data['answered_pertanyaan'] = $answeredPertanyaan;
    
                $data['title'] = 'Kuisioner';
                $data['kuisioner_id'] = $kuisioner_id;
                $this->load->view('templates/header', $data);
                $this->load->view('templates/sidebar', $data);
                $this->load->view('templates/topbar_1', $data);
                $this->load->view('kuisioner/jawaban/jawab', $data);
                $this->load->view('templates/footer');
            } else {
                // Prepare the directory to store uploaded files
                $upload_path = './uploads/';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777, true);
                }
    
                // Process and store uploaded files
                $file_uploads = $_FILES['file_upload'];
                $uploaded_files = [];
    
                foreach ($file_uploads['name'] as $key => $filename) {
                    if ($file_uploads['error'][$key] === UPLOAD_ERR_OK) {
                        $tmp_name = $file_uploads['tmp_name'][$key];
                        $file_name = uniqid() . '_' . $filename; // Generate a unique file name
                        $file_path = $upload_path . $file_name;
    
                        if (move_uploaded_file($tmp_name, $file_path)) {
                            $uploaded_files[$key] = $file_path;
                        }
                    }
                }
    
                // Ambil semua jawaban dari input form
                $jawaban = $this->input->post('jawaban');
                $skor = $this->input->post('skor'); // Retrieve 'skor' values
    
                // Simpan jawaban kuisioner, 'skor', dan file paths ke database
                foreach ($jawaban as $pertanyaan_id => $jawaban_value) {
                    $skor_value = $skor[$pertanyaan_id]; // Get the corresponding 'skor' value
                    $file_path = isset($uploaded_files[$pertanyaan_id]) ? $uploaded_files[$pertanyaan_id] : null;
                    $this->Kuisioner_jawaban_model->submit_jawaban($pertanyaan_id, $user_id, $jawaban_value, $skor_value, $file_path);
                }
    
                // Redirect kembali ke halaman sebelumnya atau halaman lain yang sesuai
                redirect('kuisioner');
            }
        } catch (Exception $e) {
            // Handle the exception, e.g., log the error or display an error message
            $error_message = $e->getMessage();
            log_message('error', $error_message);
        }
    }
    
    
    


    public function view($kuisioner_id, $user_id)
    {
        // Mendapatkan jawaban kuisioner berdasarkan kuisioner_id dan user_id
        $data['user'] = $this->db->get_where('user', ['user_id' =>
        $this->session->userdata('user_id')])->row_array();
        $data['user_data'] = $this->db->get_where('user_data', ['data_id' =>
        $data['user']['data_id']])->row_array();
        $data['admin'] = $this->db->get_where('admin', ['dos_id' =>
        $data['user']['dos_id']])->row_array();
        $data['jawaban'] = $this->Kuisioner_jawaban_model->get_jawaban_by_user_id($kuisioner_id, $user_id);
    
        // Mendapatkan informasi kuisioner (judul, deskripsi, kategori)
        $kuisioner_info = $this->Kuisioner_model->get_kuisioner_by_id($kuisioner_id);
    
        $data['kuisioner'] = $kuisioner_info;
        $data['title'] = 'Detail Jawaban Kuisioner';
        
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar_1', $data);
        $this->load->view('kuisioner/jawaban/view', $data); // Buat view yang sesuai
        $this->load->view('templates/footer');
    }
    
    
}
