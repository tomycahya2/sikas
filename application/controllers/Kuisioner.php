<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kuisioner extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Kuisioner_model'); // Load model Kuisioner_model
        $this->load->model('Kuisioner_kategori_model');
        $this->load->model('Kuisioner_pertanyaan_model');
        $this->load->model('Kuisioner_jawaban_model');
    }

    public function index()
    {
        // Mendapatkan daftar kuisioner dari database
        $data['kuisioner'] = $this->Kuisioner_model->get_all_kuisioner();
        // Tampilkan view daftar kuisioner
        $data['user'] = $this->db->get_where('user', ['user_id' =>
        $this->session->userdata('user_id')])->row_array();
        $data['user_data'] = $this->db->get_where('user_data', ['data_id' =>
        $data['user']['data_id']])->row_array();
        $data['admin'] = $this->db->get_where('admin', ['dos_id' =>
        $data['user']['dos_id']])->row_array();
        
        $data['title'] = 'Pertanyaan';
        // Tampilkan view daftar pertanyaan kuisioner

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar_1', $data);
        $this->load->view('kuisioner/index', $data);
        $this->load->view('templates/footer');
    }

    public function create()
    {
        $data['user'] = $this->db->get_where('user', ['user_id' =>
        $this->session->userdata('user_id')])->row_array();
        $data['user_data'] = $this->db->get_where('user_data', ['data_id' =>
        $data['user']['data_id']])->row_array();
        $data['admin'] = $this->db->get_where('admin', ['dos_id' =>
        $data['user']['dos_id']])->row_array();
        $data['kategori'] = $this->Kuisioner_kategori_model->get_all_kategori();
        // Tampilkan form untuk membuat kuisioner baru
        $data['title'] = 'Create';
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar_1', $data);
        $this->load->view('kuisioner/create', $data);
        $this->load->view('templates/footer');
    }

    public function store()
    {
        // Validasi form
        $this->form_validation->set_rules('judul', 'Judul', 'required');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required');
        $this->form_validation->set_rules('kategori_id', 'Kategori', 'required');

        if ($this->form_validation->run() === FALSE) {
            // Jika validasi gagal, tampilkan kembali form
            $data['kategori'] = $this->Kuisioner_kategori_model->get_all_kategori();
            $this->load->view('kuisioner/create', $data);
        } else {
            // Simpan data kuisioner ke database
            $this->Kuisioner_model->create_kuisioner();
            // Redirect kembali ke daftar kuisioner
            redirect('kuisioner');
        }
    }

    public function edit($id)
    {
        // Mendapatkan data kuisioner berdasarkan ID
        $data['user'] = $this->db->get_where('user', ['user_id' =>
        $this->session->userdata('user_id')])->row_array();
        $data['user_data'] = $this->db->get_where('user_data', ['data_id' =>
        $data['user']['data_id']])->row_array();
        $data['admin'] = $this->db->get_where('admin', ['dos_id' =>
        $data['user']['dos_id']])->row_array();
        $data['kuisioner'] = $this->Kuisioner_model->get_kuisioner_by_id($id);
        $data['kategori'] = $this->Kuisioner_kategori_model->get_all_kategori();
        // Tampilkan form edit kuisioner
        $data['title'] = 'Edit';

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar_1', $data);
        $this->load->view('kuisioner/edit', $data);
        $this->load->view('templates/footer');
    }

    public function update($id)
    {
        // Validasi form
        $this->form_validation->set_rules('judul', 'Judul', 'required');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required');
        $this->form_validation->set_rules('kategori_id', 'Kategori', 'required');

        if ($this->form_validation->run() === FALSE) {
            // Jika validasi gagal, tampilkan kembali form edit
            $data['kategori'] = $this->Kuisioner_kategori_model->get_all_kategori();
            $data['kuisioner'] = $this->Kuisioner_model->get_kuisioner_by_id($id);

            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar_1', $data);
            $this->load->view('kuisioner/edit', $data);
            $this->load->view('templates/footer');
        } else {
            // Update data kuisioner ke database
            $this->Kuisioner_model->update_kuisioner($id);
            // Redirect kembali ke daftar kuisioner
            redirect('kuisioner');
        }
    }
    
    // Kuisioner delete
    public function delete($id)
    {
        try {
            // Hapus pertanyaan-pertanyaan terkait yang memiliki kuisioner_id yang sama dengan kuisioner yang akan dihapus
            $this->Kuisioner_pertanyaan_model->delete_by_kuisioner_id($id);

            // Hapus data kuisioner berdasarkan ID
            $this->Kuisioner_model->delete_kuisioner($id);

            // Redirect kembali ke daftar kuisioner
            redirect('kuisioner');
        } catch (Exception $e) {
            echo 'Terjadi kesalahan: ' . $e->getMessage();
        }
    }

    public function detail($id)
    {
        $data['user'] = $this->db->get_where('user', ['user_id' =>
        $this->session->userdata('user_id')])->row_array();
        $data['user_data'] = $this->db->get_where('user_data', ['data_id' =>
        $data['user']['data_id']])->row_array();
        $data['admin'] = $this->db->get_where('admin', ['dos_id' =>
        $data['user']['dos_id']])->row_array();
        // Mendapatkan data kuisioner berdasarkan ID
        $data['kuisioner'] = $this->Kuisioner_model->get_kuisioner_by_id($id);
    
        // Mendapatkan daftar pertanyaan kuisioner berdasarkan kuisioner ID
        $data['pertanyaan'] = $this->Kuisioner_pertanyaan_model->get_pertanyaan_by_kuisioner_id($id);
    
        // Mendapatkan daftar pengguna yang telah menjawab pertanyaan kuisioner
        $data['users'] = $this->Kuisioner_jawaban_model->get_users_by_kuisioner_id($id);
    
        $data['title'] = 'Detail Kuisioner';
    
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar_1', $data);
        $this->load->view('kuisioner/detail', $data);
        $this->load->view('templates/footer');
    }
    
    public function print($kuisioner_id, $user_id)
    {
        // panggil library yang kita buat sebelumnya yang bernama pdfgenerator
        $this->load->library('pdf');
        
        // Mendapatkan informasi kuisioner (judul, deskripsi, kategori)
        $kuisioner_info = $this->Kuisioner_model->get_kuisioner_by_id2($kuisioner_id);
    
        $data['kuisioner'] = $kuisioner_info;
        $data['jawaban'] = $this->Kuisioner_jawaban_model->get_jawaban_by_user_id($kuisioner_id, $user_id);
        // print_r($data['kuisioner']);exit;
        // filename dari pdf ketika didownload
        $file_pdf = 'Hasil Kuisioner';
        // setting paper
        $paper = 'A4';
        //orientasi paper potrait / landscape
        $orientation = "portrait";
        
		$html = $this->load->view('kuisioner/print/GeneratePdfView',$data, true);	    
        
        // run dompdf
        $this->pdf->generate($html, $file_pdf,$paper,$orientation);
    }
    public function export($kuisioner_id, $user_id)
	{
        // Load plugin PHPExcel nya
		include APPPATH.'third_party/PHPExcel/PHPExcel.php';

		// Panggil class PHPExcel nya
		$excel = new PHPExcel();

		// Settingan awal fil excel
		$excel->getProperties()->setCreator('Data Kuisoner')
		->setLastModifiedBy($user_id)
		->setTitle("Laporan Hasil Kuisioner")
		->setSubject("Data Kuisoner")
		->setDescription("Laporan Hasil Kuisioner")
		->setKeywords("Data Kuisoner");

		// Buat sebuah variabel untuk menampung pengaturan style dari header tabel
		$style_col = array(
			'font' => array('bold' => true), // Set font nya jadi bold
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
			),
			'borders' => array(
				'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
				'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
				'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
				'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
				)
			);

			// Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
			$style_row = array(
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
				),
				'borders' => array(
					'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
					'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
					'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
					'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
					)
				);
			$style_row2 = array(
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP // Set text jadi di tengah secara vertical (middle)
					),
					'borders' => array(
						// 'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
						'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
						'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
						'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
						)
					);
			
			$style_right = array(
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT // Set text jadi di tengah secara vertical (middle)
					),
					'borders' => array(
						'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
						'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
						'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
						'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
						)
					);

				$excel->setActiveSheetIndex(0)->setCellValue('A1', "Hasil Kuisioner"); // Set kolom A1 dengan tulisan "DATA SISWA"
				$excel->getActiveSheet()->mergeCells('A1:G1'); // Set Merge Cell pada kolom A1 sampai E1
				$excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
				$excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
				$excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

				// Buat header tabel nya pada baris ke 3
                // Mendapatkan informasi kuisioner (judul, deskripsi, kategori)
                $excel->setActiveSheetIndex(0)->setCellValue('D3', "Judul"); // Set kolom A3 dengan tulisan "NO"
				$excel->setActiveSheetIndex(0)->setCellValue('D4', "Deskripsi"); // Set kolom A3 dengan tulisan "NO"
				$excel->setActiveSheetIndex(0)->setCellValue('D5', "Kategori"); // Set kolom B3 dengan tulisan "NIS"
                $kuisioner_info = $this->Kuisioner_model->get_kuisioner_by_id2($kuisioner_id);
            
                $kuisioner = $kuisioner_info;
                $no = 1; // Untuk penomoran tabel, di awal set dengan 1
				$numrow = 3; // Set baris pertama untuk isi tabel adalah baris ke 4
				foreach ($kuisioner as $item){ // Lakukan looping pada variabel siswa
					$excel->setActiveSheetIndex(0)->setCellValue('E3', $item->judul);
                    $excel->setActiveSheetIndex(0)->setCellValue('E4', $item->deskripsi);
                    $excel->setActiveSheetIndex(0)->setCellValue('E5', $item->kategori);
					// kolom ttd

					// $no++; // Tambah 1 setiap kali looping
					// $numrow++; // Tambah 1 setiap kali looping
				}
                
                $excel->setActiveSheetIndex(0)->setCellValue('C7', "Pertanyaan"); // Set kolom A3 dengan tulisan "NO"
				$excel->setActiveSheetIndex(0)->setCellValue('D7', "Jawaban"); // Set kolom A3 dengan tulisan "NO"
				$excel->setActiveSheetIndex(0)->setCellValue('E7', "Skor");
                $excel->setActiveSheetIndex(0)->setCellValue('F7', "Keterangan");

				// Apply style header yang telah kita buat tadi ke masing-masing kolom header
				$excel->getActiveSheet()->getStyle('C7')->applyFromArray($style_col);
				$excel->getActiveSheet()->getStyle('D7')->applyFromArray($style_col);
				$excel->getActiveSheet()->getStyle('E7')->applyFromArray($style_col);
				$excel->getActiveSheet()->getStyle('F7')->applyFromArray($style_col);

                $jawaban = $this->Kuisioner_jawaban_model->get_jawaban_by_user_id($kuisioner_id, $user_id);
				$no = 1; // Untuk penomoran tabel, di awal set dengan 1
				$numrow = 8; // Set baris pertama untuk isi tabel adalah baris ke 4
				foreach($jawaban as $data){ // Lakukan looping pada variabel siswa
                    $keterangan = "";
                        if ($data->skor < 40) {
                            $keterangan = "KURANG";
                        } elseif ($data->skor >= 40 && $data->skor < 60) {
                            $keterangan = "CUKUP";
                        } elseif ($data->skor >= 60 && $data->skor < 80) {
                            $keterangan = "BAIK";
                        } elseif ($data->skor >= 80) {
                            $keterangan = "BAIK SEKALI";
                        }
                        // echo $keterangan;
					$excel->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $data->pertanyaan);
					$excel->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $data->jawaban);
					$excel->setActiveSheetIndex(0)->setCellValue('E'.$numrow, $data->skor);
					$excel->setActiveSheetIndex(0)->setCellValue('F'.$numrow, $keterangan);

					// Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
					$excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);
					$excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);
					$excel->getActiveSheet()->getStyle('E'.$numrow)->applyFromArray($style_row);
					$excel->getActiveSheet()->getStyle('F'.$numrow)->applyFromArray($style_row);

					$no++; // Tambah 1 setiap kali looping
					$numrow++; // Tambah 1 setiap kali looping
				}
				
				$excel->getActiveSheet()->getColumnDimension('A')->setWidth(5); // Set width kolom A
				$excel->getActiveSheet()->getColumnDimension('B')->setWidth(15); // Set width kolom B
				$excel->getActiveSheet()->getColumnDimension('C')->setWidth(25); // Set width kolom C
				$excel->getActiveSheet()->getColumnDimension('D')->setWidth(20); // Set width kolom D
				$excel->getActiveSheet()->getColumnDimension('E')->setWidth(30); // Set width kolom E
				$excel->getActiveSheet()->getColumnDimension('F')->setWidth(30); // Set width kolom E
				$excel->getActiveSheet()->getColumnDimension('G')->setWidth(30); // Set width kolom E

				// Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
				$excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);

				// Set orientasi kertas jadi LANDSCAPE
				$excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

				// Set judul file excel nya
				$excel->getActiveSheet(0)->setTitle("Laporan Hasil Kuisioner");
				$excel->setActiveSheetIndex(0);

				// Proses file excel
				header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
				header('Content-Disposition: attachment; filename="Laporan Hasil Kuisioner.xls"'); // Set nama file excel nya
				header('Cache-Control: max-age=0');

				$write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
				$write->save('php://output');
    }
}
