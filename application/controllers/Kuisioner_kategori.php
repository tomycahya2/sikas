<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kuisioner_kategori extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Kuisioner_kategori_model');
        $this->load->model('Kuisioner_pertanyaan_model');
        $this->load->model('Kuisioner_model'); // Load model Kuisioner_kategori_model
    }

    public function index()
    {
        // Mendapatkan daftar kategori kuisioner dari database
        $data['user'] = $this->db->get_where('user', ['user_id' =>
        $this->session->userdata('user_id')])->row_array();
        $data['user_data'] = $this->db->get_where('user_data', ['data_id' =>
        $data['user']['data_id']])->row_array();
        $data['admin'] = $this->db->get_where('admin', ['dos_id' =>
        $data['user']['dos_id']])->row_array();
        $data['kategori'] = $this->Kuisioner_kategori_model->get_all_kategori();
        // Tampilkan view daftar kategori kuisioner
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar_1', $data);
        $this->load->view('kuisioner/kategori/index', $data);
        $this->load->view('templates/footer');
    }

    public function create()
    {
        // Tampilkan form untuk membuat kategori kuisioner baru
        $data['user'] = $this->db->get_where('user', ['user_id' =>
        $this->session->userdata('user_id')])->row_array();
        $data['user_data'] = $this->db->get_where('user_data', ['data_id' =>
        $data['user']['data_id']])->row_array();
        $data['admin'] = $this->db->get_where('admin', ['dos_id' =>
        $data['user']['dos_id']])->row_array();
        $data['title'] = 'Buat Kategori';
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar_1', $data);
        $this->load->view('kuisioner/kategori/create', $data);
        $this->load->view('templates/footer');
    }

    public function store()
    {
        // Validasi form
        $this->form_validation->set_rules('kategori', 'Kategori', 'required');

        if ($this->form_validation->run() === FALSE) {
            // Jika validasi gagal, tampilkan kembali form
            $data['title'] = 'Buat Kategori';
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar_1', $data);
            $this->load->view('kuisioner/kategori/create', $data);
            $this->load->view('templates/footer');
        } else {
            // Simpan data kategori kuisioner ke database
            $this->Kuisioner_kategori_model->create_kategori();
            // Redirect kembali ke daftar kategori kuisioner
            redirect('kuisioner_kategori');
        }
    }

    public function edit($id)
    {
        // Mendapatkan data kategori kuisioner berdasarkan ID
        $data['user'] = $this->db->get_where('user', ['user_id' =>
        $this->session->userdata('user_id')])->row_array();
        $data['user_data'] = $this->db->get_where('user_data', ['data_id' =>
        $data['user']['data_id']])->row_array();
        $data['admin'] = $this->db->get_where('admin', ['dos_id' =>
        $data['user']['dos_id']])->row_array();
        $data['kategori'] = $this->Kuisioner_kategori_model->get_kategori_by_id($id);
        // Tampilkan form edit kategori kuisioner
        $data['title'] = 'Edit';
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar_1', $data);
        $this->load->view('kuisioner/kategori/edit', $data);
        $this->load->view('templates/footer');
    }

    public function update($id)
    {
        // Validasi form
        $this->form_validation->set_rules('kategori', 'Kategori', 'required');

        if ($this->form_validation->run() === FALSE) {
            // Jika validasi gagal, tampilkan kembali form edit
            $data['kategori'] = $this->Kuisioner_kategori_model->get_kategori_by_id($id);
            $data['title'] = 'Edit';
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar_1', $data);
            $this->load->view('kuisioner/kategori/edit', $data);
            $this->load->view('templates/footer');
        } else {
            // Update data kategori kuisioner ke database
            $this->Kuisioner_kategori_model->update_kategori($id);
            // Redirect kembali ke daftar kategori kuisioner
            redirect('kuisioner_kategori');
        }
    }

    // Kuisioner Kategori Delete 
    public function delete($id)
    {
        try {
            // Ambil kuisioner_id dari kategori yang akan dihapus
            $kategori = $this->Kuisioner_kategori_model->get_kategori_by_id($id);
            $kuisioner_id = $kategori->kuisioner_id ?? '';

            // Hapus data pertanyaan kuisioner terkait yang memiliki kuisioner_id sesuai dengan kategori yang akan dihapus
            $this->Kuisioner_pertanyaan_model->delete_by_kuisioner_id($kuisioner_id);

            // Hapus data kuisioner terkait dalam tabel kuisioner berdasarkan kategori_id
            $this->Kuisioner_model->delete_by_kategori_id($id);

            // Hapus data kategori kuisioner berdasarkan ID
            $this->Kuisioner_kategori_model->delete_kategori($id);

            // Redirect kembali ke daftar kategori kuisioner
            redirect('kuisioner_kategori');
        } catch (Exception $e) {
            echo 'Terjadi kesalahan: ' . $e->getMessage();
        }
    }
}
