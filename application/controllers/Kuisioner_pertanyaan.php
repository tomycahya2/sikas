<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kuisioner_pertanyaan extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Kuisioner_pertanyaan_model'); // Load model Kuisioner_pertanyaan_model
    }
    public function index($kuisioner_id)
    {
        // Mendapatkan daftar pertanyaan kuisioner dari database berdasarkan kuisioner_id
        $data['user'] = $this->db->get_where('user', ['user_id' =>
        $this->session->userdata('user_id')])->row_array();
        $data['user_data'] = $this->db->get_where('user_data', ['data_id' =>
        $data['user']['data_id']])->row_array();
        $data['admin'] = $this->db->get_where('admin', ['dos_id' =>
        $data['user']['dos_id']])->row_array();
        $data['pertanyaan'] = $this->Kuisioner_pertanyaan_model->get_all_pertanyaan($kuisioner_id);
     
        // Mengambil data kuisioner berdasarkan kuisioner_id
        $this->db->where('id', $kuisioner_id);
        $data['kuisioner'] = $this->db->get('kuisioner')->row();
        $data['title'] = 'Pertanyaan';
        // Tampilkan view daftar pertanyaan kuisioner

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar_1', $data);
        $this->load->view('kuisioner/pertanyaan/index', $data);
        $this->load->view('templates/footer');
    }


    public function create($kuisioner_id)
    {
        // Tampilkan form untuk membuat pertanyaan kuisioner baru
        $data['user'] = $this->db->get_where('user', ['user_id' =>
        $this->session->userdata('user_id')])->row_array();
        $data['user_data'] = $this->db->get_where('user_data', ['data_id' =>
        $data['user']['data_id']])->row_array();
        $data['admin'] = $this->db->get_where('admin', ['dos_id' =>
        $data['user']['dos_id']])->row_array();
        $data['kuisioner_id'] = $kuisioner_id;
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar_1', $data);
        $this->load->view('kuisioner/pertanyaan/create', $data);
        $this->load->view('templates/footer');
    }

    public function store()
    {
        // Validasi form
        $this->form_validation->set_rules('pertanyaan', 'Pertanyaan', 'required');
        $kuisioner_id = $this->input->post('kuisioner_id');

        if ($this->form_validation->run() === FALSE) {
            // Jika validasi gagal, tampilkan kembali form
            $data['kuisioner_id'] = $kuisioner_id;
            $this->load->view('kuisioner/pertanyaan/create', $data);
        } else {
            // Simpan data pertanyaan kuisioner ke database
            $data = array(
                'kuisioner_id' => $kuisioner_id,
                'pertanyaan' => $this->input->post('pertanyaan')
            );
            $this->Kuisioner_pertanyaan_model->create_pertanyaan($data);
            // Redirect kembali ke daftar pertanyaan kuisioner
            redirect('kuisioner_pertanyaan/index/' . $kuisioner_id);
        }
    }

    public function edit($id)
    {
        // Mendapatkan data pertanyaan kuisioner berdasarkan ID
        $data['user'] = $this->db->get_where('user', ['user_id' =>
        $this->session->userdata('user_id')])->row_array();
        $data['user_data'] = $this->db->get_where('user_data', ['data_id' =>
        $data['user']['data_id']])->row_array();
        $data['admin'] = $this->db->get_where('admin', ['dos_id' =>
        $data['user']['dos_id']])->row_array();
        $data['pertanyaan'] = $this->Kuisioner_pertanyaan_model->get_pertanyaan_by_id($id);
        // Tampilkan form edit pertanyaan kuisioner
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar_1', $data);
        $this->load->view('kuisioner/pertanyaan/edit', $data);
        $this->load->view('templates/footer');
    }

    public function update($id)
    {
        // Validasi form
        $this->form_validation->set_rules('pertanyaan', 'Pertanyaan', 'required');

        if ($this->form_validation->run() === FALSE) {
            // Jika validasi gagal, tampilkan kembali form edit
            $data['pertanyaan'] = $this->Kuisioner_pertanyaan_model->get_pertanyaan_by_id($id);
            $this->load->view('kuisioner/pertanyaan/edit', $data);
        } else {
            // Update data pertanyaan kuisioner ke database
            $data = array(
                'pertanyaan' => $this->input->post('pertanyaan')
            );
            $this->Kuisioner_pertanyaan_model->update_pertanyaan($id, $data);
            // Redirect kembali ke daftar pertanyaan kuisioner
            redirect('kuisioner_pertanyaan/index/' . $this->input->post('kuisioner_id'));
        }
    }

    public function delete($id)
    {
        // Hapus data pertanyaan kuisioner berdasarkan ID
        $pertanyaan = $this->Kuisioner_pertanyaan_model->get_pertanyaan_by_id($id);
        $kuisioner_id = $pertanyaan->kuisioner_id;
        $this->Kuisioner_pertanyaan_model->delete_pertanyaan($id);
        // Redirect kembali ke daftar pertanyaan kuisioner
        redirect('kuisioner_pertanyaan/index/' . $kuisioner_id);
    }
}
