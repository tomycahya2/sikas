<?php
defined('BASEPATH') or exit('No direct script access allowed');


$route['default_controller'] = 'Landingpage';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

// Rute untuk pertanyaan kuisioner
$route['kuisioner/pertanyaan/(:any)'] = 'Kuisioner_pertanyaan/index/$1';
$route['kuisioner/pertanyaan/create/(:any)'] = 'Kuisioner_pertanyaan/create/$1';
$route['kuisioner/pertanyaan/store'] = 'Kuisioner_pertanyaan/store';
$route['kuisioner/pertanyaan/edit/(:any)'] = 'Kuisioner_pertanyaan/edit/$1';
$route['kuisioner/pertanyaan/update/(:any)'] = 'Kuisioner_pertanyaan/update/$1';
$route['kuisioner/pertanyaan/delete/(:any)'] = 'Kuisioner_pertanyaan/delete/$1';

$route['kuisioner/jawaban/view/(:num)/(:num)'] = 'Kuisioner_jawab/view/$1/$2';
$route['kuisioner/jawaban/(:any)'] = 'Kuisioner_jawab/jawab/$1';
$route['kuisioner/jawab_submit'] = 'Kuisioner_jawab/submit_jawaban';

// Rute untuk kategori kuisioner
$route['kuisioner/kategori'] = 'Kuisioner_kategori/index';
$route['kuisioner/kategori/create'] = 'Kuisioner_kategori/create';
$route['kuisioner/kategori/store'] = 'Kuisioner_kategori/store';
$route['kuisioner/kategori/edit/(:any)'] = 'Kuisioner_kategori/edit/$1';
$route['kuisioner/kategori/update/(:any)'] = 'Kuisioner_kategori/update/$1';
$route['kuisioner/kategori/delete/(:any)'] = 'Kuisioner_kategori/delete/$1';

// Rute untuk kuisioner
$route['kuisioner'] = 'Kuisioner/index';
$route['kuisioner/create'] = 'Kuisioner/create';
$route['kuisioner/store'] = 'Kuisioner/store';
$route['kuisioner/edit/(:any)'] = 'Kuisioner/edit/$1';
$route['kuisioner/update/(:any)'] = 'Kuisioner/update/$1';
$route['kuisioner/delete/(:any)'] = 'Kuisioner/delete/$1';
