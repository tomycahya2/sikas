<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kuisioner_model extends CI_Model
{
    public function get_all_kuisioner()
    {
        // Menentukan kolom yang akan dipilih
        $this->db->select('kuisioner.*, kuisioner_kategori.kategori, COUNT(DISTINCT kuisioner_jawaban.user_id) as jumlah_pengguna, COUNT(DISTINCT kuisioner_pertanyaan.id) as jumlah_pertanyaan');
        // Menggabungkan tabel kuisioner dengan tabel kuisioner_kategori berdasarkan kategori_id
        $this->db->join('kuisioner_kategori', 'kuisioner.kategori_id = kuisioner_kategori.id', 'left');
        // Menggabungkan tabel kuisioner dengan tabel kuisioner_pertanyaan berdasarkan kuisioner_id
        $this->db->join('kuisioner_pertanyaan', 'kuisioner.id = kuisioner_pertanyaan.kuisioner_id', 'left');
        // Menggabungkan tabel kuisioner_pertanyaan dengan tabel kuisioner_jawaban berdasarkan pertanyaan_id
        $this->db->join('kuisioner_jawaban', 'kuisioner_pertanyaan.id = kuisioner_jawaban.pertanyaan_id', 'left');
        // Menggunakan GROUP BY untuk mengelompokkan data kuisioner berdasarkan ID
        $this->db->group_by('kuisioner.id');

        // Ambil semua data kuisioner dari tabel kuisioner
        $query = $this->db->get('kuisioner');

        return $query->result();
    }



    public function create_kuisioner()
    {
        // Simpan data kuisioner baru ke dalam tabel kuisioner
        $data = array(
            'judul' => $this->input->post('judul'),
            'deskripsi' => $this->input->post('deskripsi'),
            'kategori_id' => $this->input->post('kategori_id')
        );
        return $this->db->insert('kuisioner', $data);
    }
    public function get_kuisioner_by_id($id)
    {
        // Ambil data kuisioner berdasarkan ID
        $this->db->select('kuisioner.*, kuisioner_kategori.kategori');
        $this->db->from('kuisioner');
        $this->db->join('kuisioner_kategori', 'kuisioner.kategori_id = kuisioner_kategori.id', 'left');
        $this->db->where('kuisioner.id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    public function get_kuisioner_by_id2($id)
    {
        // Ambil data kuisioner berdasarkan ID
        $this->db->select('kuisioner.*, kuisioner_kategori.kategori');
        $this->db->from('kuisioner');
        $this->db->join('kuisioner_kategori', 'kuisioner.kategori_id = kuisioner_kategori.id', 'left');
        $this->db->where('kuisioner.id', $id);
        $query = $this->db->get();
        return $query->result();
    }


    public function update_kuisioner($id)
    {
        // Update data kuisioner berdasarkan ID
        $data = array(
            'judul' => $this->input->post('judul'),
            'deskripsi' => $this->input->post('deskripsi'),
            'kategori_id' => $this->input->post('kategori_id')
        );
        $this->db->where('id', $id);
        return $this->db->update('kuisioner', $data);
    }

    public function delete_kuisioner($id)
    {
        // Hapus data kuisioner berdasarkan ID
        $this->db->where('id', $id);
        return $this->db->delete('kuisioner');
    }

    public function delete_by_kategori_id($kategori_id)
    {
        // Hapus data kuisioner berdasarkan kategori_id
        $this->db->where('kategori_id', $kategori_id);
        $this->db->delete('kuisioner');
    }
}
