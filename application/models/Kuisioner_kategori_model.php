<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kuisioner_kategori_model extends CI_Model {

    public function get_all_kategori() {
        // Ambil semua data kategori kuisioner dari tabel kuisioner_kategori
        $query = $this->db->get('kuisioner_kategori');
        return $query->result();
    }

    public function create_kategori() {
        // Simpan data kategori kuisioner baru ke dalam tabel kuisioner_kategori
        $data = array(
            'kategori' => $this->input->post('kategori')
        );
        return $this->db->insert('kuisioner_kategori', $data);
    }

    public function get_kategori_by_id($id) {
        // Ambil data kategori kuisioner berdasarkan ID
        $query = $this->db->get_where('kuisioner_kategori', array('id' => $id));
        return $query->row();
    }

    public function update_kategori($id) {
        // Update data kategori kuisioner berdasarkan ID
        $data = array(
            'kategori' => $this->input->post('kategori')
        );
        $this->db->where('id', $id);
        return $this->db->update('kuisioner_kategori', $data);
    }

    public function delete_kategori($id) {
        // Hapus data kategori kuisioner berdasarkan ID
        $this->db->where('id', $id);
        return $this->db->delete('kuisioner_kategori');
    }
}
