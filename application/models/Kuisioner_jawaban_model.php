<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kuisioner_jawaban_model extends CI_Model
{

    public function get_pertanyaan_by_kuisioner_id($kuisioner_id)
    {
        // Ambil daftar pertanyaan kuisioner berdasarkan kuisioner_id
        $this->db->where('kuisioner_id', $kuisioner_id);
        $query = $this->db->get('kuisioner_pertanyaan');
        return $query->result();
    }

    public function submit_jawaban($pertanyaan_id, $user_id, $jawaban, $skor, $file_upload = null)
    {
        // Simpan jawaban kuisioner ke database
        $data = array(
            'jawaban' => $jawaban,
            'skor' => $skor,
            'pertanyaan_id' => $pertanyaan_id,
            'user_id' => $user_id,
            'file_upload' => $file_upload // Add the file_upload field
        );
        $this->db->insert('kuisioner_jawaban', $data);
    }



    public function hasAnswered($pertanyaan_id, $user_id)
    {
        // Mengecek apakah pengguna dengan $user_id sudah menjawab pertanyaan dengan $pertanyaan_id
        $this->db->where('pertanyaan_id', $pertanyaan_id);
        $this->db->where('user_id', $user_id);
        $query = $this->db->get('kuisioner_jawaban');

        // Mengembalikan true jika sudah menjawab, false jika belum
        return $query->num_rows() > 0;
    }

    public function get_users_by_kuisioner_id($kuisioner_id)
    {
        // Select necessary fields including user_id and calculate the total score
        $this->db->select('kuisioner_jawaban.user_id, user.id, user.user_id as userId, SUM(kuisioner_jawaban.skor) as total_skor');
    
        // Join relevant tables
        $this->db->from('kuisioner_jawaban');
        $this->db->join('kuisioner_pertanyaan', 'kuisioner_jawaban.pertanyaan_id = kuisioner_pertanyaan.id', 'left');
        $this->db->join('kuisioner', 'kuisioner_pertanyaan.kuisioner_id = kuisioner.id', 'left');
        $this->db->join('kuisioner_kategori', 'kuisioner.kategori_id = kuisioner_kategori.id', 'left');
        $this->db->join('user', 'kuisioner_jawaban.user_id = user.id', 'left');
    
        // Add a WHERE clause to filter by kuisioner ID
        $this->db->where('kuisioner.id', $kuisioner_id);
    
        // Group by user_id to calculate the total score for each user
        $this->db->group_by('kuisioner_jawaban.user_id');
    
        // Get the query result
        $query = $this->db->get();
    
        // Fetch the results as an array of objects
        $users = $query->result();
    
        return $users;
    }
    

    public function get_jawaban_by_user_id($kuisioner_id, $user_id)
    {
        $this->db->select('kuisioner_pertanyaan.pertanyaan, kuisioner_jawaban.jawaban, kuisioner_jawaban.file_upload, kuisioner_jawaban.skor');
        $this->db->from('kuisioner_jawaban');
        $this->db->join('kuisioner_pertanyaan', 'kuisioner_jawaban.pertanyaan_id = kuisioner_pertanyaan.id');
        $this->db->where('kuisioner_pertanyaan.kuisioner_id', $kuisioner_id);
        $this->db->where('kuisioner_jawaban.user_id', $user_id);
        $query = $this->db->get();

        return $query->result();
    }
}
