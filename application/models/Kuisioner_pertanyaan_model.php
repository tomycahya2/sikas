<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kuisioner_pertanyaan_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        // Load library database CodeIgniter
        $this->load->database();
    }

    public function get_all_pertanyaan($kuisioner_id)
    {
        // Ambil semua data pertanyaan kuisioner dari tabel 'kuisioner_pertanyaan' berdasarkan kuisioner_id
        return $this->db->get_where('kuisioner_pertanyaan', array('kuisioner_id' => $kuisioner_id))->result();
    }

    public function create_pertanyaan($data)
    {
        // Insert data ke dalam tabel 'kuisioner_pertanyaan'
        return $this->db->insert('kuisioner_pertanyaan', $data);
    }

    public function get_pertanyaan_by_id($id)
    {
        // Ambil data pertanyaan kuisioner berdasarkan ID
        return $this->db->get_where('kuisioner_pertanyaan', array('id' => $id))->row();
    }

    public function update_pertanyaan($id, $data)
    {
        // Update data pertanyaan kuisioner berdasarkan ID
        $this->db->where('id', $id);
        return $this->db->update('kuisioner_pertanyaan', $data);
    }

    public function delete_pertanyaan($id)
    {
        // Hapus data pertanyaan kuisioner berdasarkan ID
        return $this->db->delete('kuisioner_pertanyaan', array('id' => $id));
    }

    public function delete_by_kuisioner_id($kuisioner_id)
    {
        // Hapus data pertanyaan kuisioner berdasarkan kuisioner_id
        $this->db->where('kuisioner_id', $kuisioner_id);
        $this->db->delete('kuisioner_pertanyaan');
    }

    public function get_pertanyaan_by_kuisioner_id($kuisioner_id)
    {
        // Ambil daftar pertanyaan kuisioner berdasarkan kuisioner_id
        $this->db->where('kuisioner_id', $kuisioner_id);
        $query = $this->db->get('kuisioner_pertanyaan');
        return $query->result();
    }
}
